
*******************************************************
    README.txt for import_invite for Drupal 7
*******************************************************

The main purpose of this module is used to find out the imported users count.
To send a notification mail to those users  using cron.

Installation Steps:
   1. Extract the module and place it in sites/all/modules/contrib.
   2. Create two user roles called 'Legacy' and 'Legacy Mail'.
   3. Set the role 'Legacy' under Automatic Role in Feed settings.
   4. Go to Legacy user Cron and fill all the required fields.
   5. In Legacy user Details , we can able to run the cron manually.
